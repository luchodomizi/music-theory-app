Aplicación dedicada al aprendizaje musical, de forma intuitiva, fácil de utilizar y de entender.

Enlace de drive con la documentación existente: https://docs.google.com/document/d/1SwHHLftUYwB4EIXPOF7HMd0QwZdY9hVS/edit?usp=sharing&ouid=103914727529841565591&rtpof=true&sd=true

Humberto Domizi

Para visualizar el deploy directamente https://musictheoryapp.netlify.app

//Obtener el año actual
const fecha = new Date();
const añoActual = fecha.getFullYear();
document.getElementById("year").textContent = añoActual;

const jqueryJSON = () => {
  $.get(`/assets/js/database.json`, (respuesta, estado) => {
    if (estado === "success") {
      const notes = respuesta[0].notes;
      const chords = respuesta[1].chords;
      const notations = respuesta[2].notations;
      const modes = respuesta[3].modes;

      const mayorChords = [];
      const minorChords = [];
      const mayorMode = [];
      const minorMode1 = [];
      const minorMode2 = [];
      const ionianScales = [];
      const dorianScales = [];
      const phrygianScales = [];
      const lydianScales = [];
      const mixolydianScales = [];
      const aeolianScales = [];
      const locrianScales = [];

      const sweetAlertProcess = () => {
        let timerInterval;
        Swal.fire({
          title: "Procesando!",
          timer: 800,
          timerProgressBar: true,
          didOpen: () => {
            Swal.showLoading();
          },
          willClose: () => {
            clearInterval(timerInterval);
          },
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        });
      };

      //Constructor de acordes, con sus métodos
      class chordMaker {
        constructor(
          name,
          first,
          bsecond,
          second,
          thirdMin,
          thirdMay,
          quarter,
          bfifth,
          fifth,
          bsixth,
          sixth,
          bseventh,
          seventh,
          bninth,
          ninth,
          eleventh,
          bthirteenth,
          thirteenth
        ) {
          (this.name = name),
            (this.first = first),
            (this.bsecond = bsecond),
            (this.second = second),
            (this.thirdMin = thirdMin),
            (this.thirdMay = thirdMay),
            (this.quarter = quarter),
            (this.bfifth = bfifth),
            (this.fifth = fifth),
            (this.bsixth = bsixth),
            (this.sixth = sixth),
            (this.bseventh = bseventh),
            (this.seventh = seventh),
            (this.bninth = bninth),
            (this.ninth = ninth),
            (this.eleventh = eleventh),
            (this.bthirteenth = bthirteenth),
            (this.thirteenth = thirteenth);
        }
        generateMayorChord() {
          return {
            name: this.name,
            first: this.first,
            second: this.second,
            thirdMay: this.thirdMay,
            quarter: this.quarter,
            fifth: this.fifth,
            sixth: this.sixth,
            seventh: this.seventh,
            ninth: this.ninth,
            eleventh: this.eleventh,
            thirteenth: this.thirteenth,
          };
        }
        generateMinorChord() {
          return {
            name: this.name + notations[1],
            first: this.first,
            second: this.second,
            thirdMin: this.thirdMin,
            quarter: this.quarter,
            fifth: this.fifth,
            bsixth: this.bsixth,
            bseventh: this.bseventh,
            ninth: this.ninth,
            eleventh: this.eleventh,
            bthirteenth: this.bthirteenth,
          };
        }
        generateMayorMode() {
          return {
            first: this.name + notations[0] + notations[9],
            second: this.second + notations[1] + notations[9],
            third: this.thirdMay + notations[1] + notations[9],
            quarter: this.quarter + notations[0] + notations[9],
            fifth: this.fifth + notations[9],
            sixth: this.sixth + notations[1] + notations[9],
            seventh: this.seventh + notations[1] + notations[9] + notations[5],
          };
        }
        generateMinorMode1() {
          return {
            first: this.name + notations[1] + notations[9],
            second: this.second + notations[1] + notations[9] + notations[5],
            third: this.thirdMin + notations[0] + notations[9],
            quarter: this.quarter + notations[1] + notations[9],
            fifth: this.fifth + notations[1] + notations[9],
            sixth: this.bsixth + notations[0] + notations[9],
            seventh: this.bseventh + notations[9],
            seventh2: this.seventh + notations[1] + notations[9] + notations[5],
          };
        }
        generateMinorMode2() {
          return {
            first: this.name + notations[1] + notations[9] + "+",
            second: this.second + notations[1] + notations[9],
            third: this.thirdMin + notations[15] + notations[0] + notations[9],
            quarter: this.quarter + notations[9],
            fifth: this.fifth + notations[9],
            sixth: this.sixth + notations[1] + notations[9] + notations[5],
            seventh: this.seventh + notations[1] + notations[9] + notations[5],
          };
        }
        generateIonianMode() {
          return {
            name: this.name + modes[0],
            first: this.first,
            second: this.second,
            third: this.thirdMay,
            quarter: this.quarter,
            fifth: this.fifth,
            sixth: this.sixth,
            seventh: this.seventh,
          };
        }
        generateDorianMode() {
          return {
            name: this.name + modes[1],
            first: this.second,
            second: this.thirdMay,
            third: this.quarter,
            quarter: this.fifth,
            fifth: this.sixth,
            sixth: this.seventh,
            seventh: this.first,
          };
        }
        generatePhrygianMode() {
          return {
            name: this.name + modes[2],
            first: this.thirdMay,
            second: this.quarter,
            third: this.fifth,
            quarter: this.sixth,
            fifth: this.seventh,
            sixth: this.first,
            seventh: this.second,
          };
        }
        generateLydianMode() {
          return {
            name: this.name + modes[3],
            first: this.quarter,
            second: this.fifth,
            third: this.sixth,
            quarter: this.seventh,
            fifth: this.first,
            sixth: this.second,
            seventh: this.thirdMay,
          };
        }
        generateMixolydianMode() {
          return {
            name: this.name + modes[4],
            first: this.fifth,
            second: this.sixth,
            third: this.seventh,
            quarter: this.first,
            fifth: this.second,
            sixth: this.thirdMay,
            seventh: this.quarter,
          };
        }
        generateAeolianMode() {
          return {
            name: this.name + modes[5],
            first: this.sixth,
            second: this.seventh,
            third: this.first,
            quarter: this.second,
            fifth: this.thirdMay,
            sixth: this.quarter,
            seventh: this.fifth,
          };
        }
        generateLocrianMode() {
          return {
            name: this.name + modes[6],
            first: this.seventh,
            second: this.first,
            third: this.second,
            quarter: this.thirdMay,
            fifth: this.quarter,
            sixth: this.fifth,
            seventh: this.sixth,
          };
        }
      }

      let name;

      //Bucle que genera array de acordes menores y mayores, y modos griegos
      for (let item of chords) {
        name = item.name;
        const first = item.degrees[0];
        const bsecond = item.degrees[1];
        const second = item.degrees[2];
        const thirdMin = item.degrees[3];
        const thirdMay = item.degrees[4];
        const quarter = item.degrees[5];
        const bfifth = item.degrees[6];
        const fifth = item.degrees[7];
        const bsixth = item.degrees[8];
        const sixth = item.degrees[9];
        const bseventh = item.degrees[10];
        const seventh = item.degrees[11];
        const bninth = item.degrees[1];
        const ninth = item.degrees[2];
        const eleventh = item.degrees[5];
        const bthirteenth = item.degrees[8];
        const thirteenth = item.degrees[9];

        const chord = new chordMaker(
          name,
          first,
          bsecond,
          second,
          thirdMin,
          thirdMay,
          quarter,
          bfifth,
          fifth,
          bsixth,
          sixth,
          bseventh,
          seventh,
          bninth,
          ninth,
          eleventh,
          bthirteenth,
          thirteenth
        );

        mayorChords.push(chord.generateMayorChord());
        minorChords.push(chord.generateMinorChord());
        mayorMode.push(chord.generateMayorMode());
        minorMode1.push(chord.generateMinorMode1());
        minorMode2.push(chord.generateMinorMode2());
        ionianScales.push(chord.generateIonianMode());
        dorianScales.push(chord.generateDorianMode());
        phrygianScales.push(chord.generatePhrygianMode());
        lydianScales.push(chord.generateLydianMode());
        mixolydianScales.push(chord.generateMixolydianMode());
        aeolianScales.push(chord.generateAeolianMode());
        locrianScales.push(chord.generateLocrianMode());
      }

      //MAYOR MODE
      function createInputMayorChord() {
        document.querySelector(
          "#selectMayorChord"
        ).innerHTML = `<select class="form-control form-control-lg text-center" id="mayor_chords_select" name="chords"></select>`;

        for (let mayorChord of mayorChords) {
          document.querySelector(
            "#mayor_chords_select"
          ).innerHTML += `<option>${mayorChord.name}</option>`;
        }
      }

      function showMayorDegreesInfo() {
        sweetAlertProcess();

        setTimeout(() => {
          selectChord = document.querySelector("#mayor_chords_select").value;

          for (i = 0; i < mayorChords.length && i < mayorMode.length; i += 1) {
            if (selectChord == mayorChords[i].name) {
              document.querySelector("#mayor_degrees").innerHTML = `  
            <tr>
            <td>${mayorChords[i].first}</td>
            <td>${mayorChords[i].second}</td>
            <td>${mayorChords[i].thirdMay}</td>
            <td>${mayorChords[i].quarter}</td>
            <td>${mayorChords[i].fifth}</td>
            <td>${mayorChords[i].sixth}</td>
            <td>${mayorChords[i].seventh}</td>
          </tr>`;

              document.querySelector("#mayorMode").innerHTML = `  
          <tr>
          <td>${mayorMode[i].first}</td>
          <td>${mayorMode[i].second}</td>
          <td>${mayorMode[i].third}</td>
          <td>${mayorMode[i].quarter}</td>
          <td>${mayorMode[i].fifth}</td>
          <td>${mayorMode[i].sixth}</td>
          <td>${mayorMode[i].seventh}</td>
        </tr>`;
            }
          }
        }, 800);
      }

      //MINOR MODE
      function createInputMinorChord() {
        document.querySelector(
          "#selectMinorChord"
        ).innerHTML = `<select class="form-control form-control-lg text-center" id="minor_chords_select" name="chords"></select>`;

        for (let minorChord of minorChords) {
          document.querySelector(
            "#minor_chords_select"
          ).innerHTML += `<option>${minorChord.name}</option>`;
        }
      }

      function showMinorDegreesInfo() {
        sweetAlertProcess();

        setTimeout(() => {
          selectChord = document.querySelector("#minor_chords_select").value;

          for (
            let i = 0;
            i < minorChords.length && minorMode1.length && minorMode2.length;
            i += 1
          ) {
            if (selectChord == minorChords[i].name) {
              document.querySelector("#minor_degrees").innerHTML = `  
            <tr>
            <td>${minorChords[i].first}</td>
            <td>${minorChords[i].second}</td>
            <td>${minorChords[i].thirdMin}</td>
            <td>${minorChords[i].quarter}</td>
            <td>${minorChords[i].fifth}</td>
            <td>${minorChords[i].bsixth}</td>
            <td>${minorChords[i].bseventh}</td>
          </tr>`;

              document.querySelector("#minorMode1").innerHTML = `  
          <tr class="table-warning">
          <td>${minorMode1[i].first}</td>
          <td>${minorMode1[i].second}</td>
          <td>${minorMode1[i].third}</td>
          <td>${minorMode1[i].quarter}</td>
          <td>${minorMode1[i].fifth}</td>
          <td>${minorMode1[i].sixth}</td>
          <td>${minorMode1[i].seventh}</td>
          <td>${minorMode1[i].seventh2}</td>
        </tr>`;
              document.querySelector("#minorMode2").innerHTML = `
        <tr class="table-info">
          <td>${minorMode2[i].first}</td>
          <td>${minorMode2[i].second}</td>
          <td>${minorMode2[i].third}</td>
          <td>${minorMode2[i].quarter}</td>
          <td>${minorMode2[i].fifth}</td>
          <td>${minorMode2[i].sixth}</td>
          <td>${minorMode2[i].seventh}</td>
          <td></td>
        </tr>`;
            }
          }
        }, 800);
      }

      //GREEKS MODES

      function createInputGreeksModes() {
        document.querySelector(
          "#selectGreeksModes"
        ).innerHTML = `<select class="form-control form-control-lg text-center" id="greeks_modes_select" name="chords"></select>`;

        for (let mayorChord of mayorChords) {
          document.querySelector(
            "#greeks_modes_select"
          ).innerHTML += `<option>${mayorChord.name}</option>`;
        }
      }

      function showGreeksModeInfo() {
        sweetAlertProcess();

        setTimeout(() => {
          selectMode = document.querySelector("#greeks_modes_select").value;

          for (i = 0; i < mayorChords.length; i += 1) {
            if (selectMode == mayorChords[i].name) {
              document.querySelector("#modes").innerHTML = `  
              <tr>
              <th class="text-primary">${mayorChords[i].first + modes[0]}</th>
              <td>${ionianScales[i].first}</td>
              <td>${ionianScales[i].second}</td>
              <td>${ionianScales[i].third}</td>
              <td>${ionianScales[i].quarter}</td>
              <td>${ionianScales[i].fifth}</td>
              <td>${ionianScales[i].sixth}</td>
              <td>${ionianScales[i].seventh}</td>
            </tr>
            <tr>
            <th class="text-secondary">${mayorChords[i].second + modes[1]}</th>
            <td>${dorianScales[i].first}</td>
            <td>${dorianScales[i].second}</td>
            <td>${dorianScales[i].third}</td>
            <td>${dorianScales[i].quarter}</td>
            <td>${dorianScales[i].fifth}</td>
            <td>${dorianScales[i].sixth}</td>
            <td>${dorianScales[i].seventh}</td>
            </tr>
            <tr>
            <th class="text-success">${mayorChords[i].thirdMay + modes[2]}</th>
            <td>${phrygianScales[i].first}</td>
            <td>${phrygianScales[i].second}</td>
            <td>${phrygianScales[i].third}</td>
            <td>${phrygianScales[i].quarter}</td>
            <td>${phrygianScales[i].fifth}</td>
            <td>${phrygianScales[i].sixth}</td>
            <td>${phrygianScales[i].seventh}</td>
            </tr>
            <tr>
            <th class="text-danger">${mayorChords[i].quarter + modes[3]}</th>
            <td>${lydianScales[i].first}</td>
            <td>${lydianScales[i].second}</td>
            <td>${lydianScales[i].third}</td>
            <td>${lydianScales[i].quarter}</td>
            <td>${lydianScales[i].fifth}</td>
            <td>${lydianScales[i].sixth}</td>
            <td>${lydianScales[i].seventh}</td>
            </tr>
            <tr>
            <th class="text-warning">${mayorChords[i].fifth + modes[4]}</th>
            <td>${mixolydianScales[i].first}</td>
            <td>${mixolydianScales[i].second}</td>
            <td>${mixolydianScales[i].third}</td>
            <td>${mixolydianScales[i].quarter}</td>
            <td>${mixolydianScales[i].fifth}</td>
            <td>${mixolydianScales[i].sixth}</td>
            <td>${mixolydianScales[i].seventh}</td>
            </tr>
            <tr>
            <th class="text-info">${mayorChords[i].sixth + modes[5]}</th>
            <td>${aeolianScales[i].first}</td>
            <td>${aeolianScales[i].second}</td>
            <td>${aeolianScales[i].third}</td>
            <td>${aeolianScales[i].quarter}</td>
            <td>${aeolianScales[i].fifth}</td>
            <td>${aeolianScales[i].sixth}</td>
            <td>${aeolianScales[i].seventh}</td>
            </tr>
            <tr>
            <th class="text-propio">${mayorChords[i].seventh + modes[6]}</th>
            <td>${locrianScales[i].first}</td>
            <td>${locrianScales[i].second}</td>
            <td>${locrianScales[i].third}</td>
            <td>${locrianScales[i].quarter}</td>
            <td>${locrianScales[i].fifth}</td>
            <td>${locrianScales[i].sixth}</td>
            <td>${locrianScales[i].seventh}</td>
            </tr>`;

              document.querySelector("#mayorMode_for_greeks").innerHTML = `  
            <tr>
            <td>${mayorMode[i].first}</td>
            <td>${mayorMode[i].second}</td>
            <td>${mayorMode[i].third}</td>
            <td>${mayorMode[i].quarter}</td>
            <td>${mayorMode[i].fifth}</td>
            <td>${mayorMode[i].sixth}</td>
            <td>${mayorMode[i].seventh}</td>
          </tr>`;
            }
          }
        }, 800);
      }

      //CONSTRUCTOR
      function createInputsConstructor() {
        for (let note of notes) {
          document.querySelector(
            "#chordFirst"
          ).innerHTML += `<option>${note}</option>`;
        }
        for (let notation of notations) {
          document.querySelector(
            "#chordNotation"
          ).innerHTML += `<option>${notation}</option>`;
        }
      }

      function generateChord() {
        chordFirst = document.querySelector("#chordFirst").value;
        chordNotation = document.querySelector("#chordNotation").value;

        document.querySelector(
          "#apiImage"
        ).innerHTML = `<ins class="scales_chords_api" chord="${chordFirst}${chordNotation}"></ins>`;
      }
      //aca termina el if de la respuesta al json

      createInputMayorChord();
      createInputMinorChord();
      createInputGreeksModes();

      $("#showMayorDeegres").on("click", showMayorDegreesInfo);
      $("#showMinorDeegres").on("click", showMinorDegreesInfo);
      $("#showGreeksModes").on("click", showGreeksModeInfo);
    }
  });
};

async function sweetAlertModal() {
  const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-right',
    iconColor: 'white',
    customClass: {
      popup: 'colored-toast'
    },
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: false
  })
  await Toast.fire({
    icon: 'success',
    title: 'PDF Descargado!'
  })
}

async function generateMayorPDF() {
  const chordName = $('#mayor_chords_select').val()

  $('#invoice').prepend(`<h2 class="text-dark display4 my-5 text-center" id="title_chord">${chordName} Mayor</h2>`);

  const content = document.querySelector('#invoice');

  html2pdf(content)

  $('#title_chord').remove();

  //SWEET ALERT MODAL
  sweetAlertModal();
}

async function generateMinorPDF() {

  $('#minor_mode_table').css({
    "font-size": "10px"

  })
  const chordName = $('#minor_chords_select').val()

  $('#invoice2').prepend(`<h2 class="text-dark display4 my-5 text-center" id="title_chord">${chordName}or</h2>`);

  const content = document.querySelector('#invoice2');


  html2pdf(content)

  $('#title_chord').remove();
  $('#minor_mode_table').css({
    "font-size": "16px"

  })

  //SWEET ALERT MODAL
  sweetAlertModal();
}

async function generateGreeksPDF() {
  const chordName = $('#greeks_modes_select').val()

  $('#invoice3').prepend(`<h2 class="text-dark display4 my-5 text-center" id="title_chord">Greeks Modes for ${chordName}</h2>`);

  const content = document.querySelector('#invoice3');


  html2pdf(content)

  $('#title_chord').remove();

  //SWEET ALERT MODAL
  sweetAlertModal();
}
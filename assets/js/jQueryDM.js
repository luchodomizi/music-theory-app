
$('#switch').on('click',() => {
    if($('#switch').is(':checked')){
        $('body').removeClass('bg-light').removeClass('text-dark').addClass('bg-dark').addClass('text-light');
        $('select').addClass('bg-dark').addClass('text-light');
        $('nav').removeClass('bg-light').addClass('bg-dark').removeClass('navbar-light').addClass('navbar-dark');
        $('table').removeClass('table-light').addClass('table-dark');
        $('.accordion-item').addClass('bg-dark');
        $('.accordion-button').addClass('bg-dark').removeClass('text-dark').addClass('text-light');
        $('.copyright').removeClass('text-muted').addClass('text-light');
        $('.footer-icon').removeClass('back-light').addClass('back-dark');

    }else{
        $('body').removeClass('bg-dark').removeClass('bg-dark').addClass('bg-light').addClass('text-dark');
        $('select').removeClass('bg-dark').removeClass('text-light');
        $('nav').removeClass('bg-dark').addClass('bg-light').removeClass('navbar-dark').addClass('navbar-light');
        $('table').removeClass('table-dark').addClass('table-light');
        $('.accordion-item').removeClass('bg-dark').addClass('bg-light');
        $('.accordion-button').removeClass('bg-dark').addClass('bg-light').addClass('text-dark');
        $('.copyright').removeClass('text-light').addClass('text-muted');
        $('.footer-icon').removeClass('back-dark').addClass('back-light');
    }
})
